import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.TimerTask;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;


public class MainGui implements ActionListener{
	public JPanel appsPanel, settingsPanel, buttonsPanel, daysPanel, preSetPanel;
	public JLabel choiceLabel, presetSettings;
	public ArrayList<String> myInstalledAppsList = new ArrayList<String>();
	public ArrayList<JCheckBox> applicationsCheckBoxes = new ArrayList<>();
    static ArrayList<String> runningApplications = new ArrayList<>();
	public ArrayList<String> checkedInstalledApplications = new ArrayList<String>();	
	public ArrayList<String> applicationsTokill = new ArrayList<String>();	
	public ArrayList<JCheckBox> weekDays = new ArrayList<>();
	public ArrayList<JCheckBox> allDaysOfWeek = new ArrayList<>();
	public ArrayList<String> days = new ArrayList<>();
	public JScrollPane scrollable;
	public int appCount = 0;
	public JCheckBox monCB, tuesCB, wedsCB, thursCB, friFB, satCB, sunCB;
	public JRadioButton yesRadio, noRadio;
	public ButtonGroup buttonGroup;
	public JButton applyButton;
	public boolean stopApps = false;
	public LocalTime rightNowTime, morningTime, eveningTime;
	public LocalDate localDate;
	public DayOfWeek dayOfWeek;	
	public JFrame frame2;
	public boolean successKill;
	
	public static void main(String[] args) {
		MainGui gui = new MainGui();
	}

	public MainGui() {
		JFrame frame = new JFrame();
		frame.setTitle("Limit");
		
		Container cp = frame.getContentPane();
		cp.setLayout(new BorderLayout());		
		
		appsPanel = new JPanel();
		appsPanel.setBorder(BorderFactory.createTitledBorder("Applications"));
		appsPanel.setLayout(new GridLayout(appCount,1));
		scrollable = new JScrollPane(appsPanel); 
		
		settingsPanel = new JPanel();
		settingsPanel.setLayout(new GridLayout(3,1));
		monCB = new JCheckBox("Monday");
		monCB.setSelected(false);
		tuesCB = new JCheckBox("Tuesday");
		tuesCB.setSelected(false);
		wedsCB = new JCheckBox("Wednesday");
		wedsCB.setSelected(false);
		thursCB = new JCheckBox("Thursday");
		thursCB.setSelected(false);
		friFB = new JCheckBox("Friday");
		friFB.setSelected(false);
		satCB = new JCheckBox("Saturday");
		satCB.setSelected(false);
		sunCB = new JCheckBox("Sunday");
		sunCB.setSelected(false);
	
		weekDays.add(monCB);
		weekDays.add(tuesCB);
		weekDays.add(wedsCB);
		weekDays.add(thursCB);
		weekDays.add(friFB);
		
		allDaysOfWeek.add(monCB);
		allDaysOfWeek.add(tuesCB);
		allDaysOfWeek.add(wedsCB);
		allDaysOfWeek.add(thursCB);
		allDaysOfWeek.add(friFB);
		allDaysOfWeek.add(satCB);
		allDaysOfWeek.add(sunCB);
		
		daysPanel = new JPanel();
		choiceLabel = new JLabel("Limit applications on:");
		daysPanel.add(choiceLabel);
		daysPanel.add(monCB);
		daysPanel.add(tuesCB);
		daysPanel.add(wedsCB);
		daysPanel.add(thursCB);
		daysPanel.add(friFB);
		daysPanel.add(satCB);
		daysPanel.add(sunCB);
		settingsPanel.add(daysPanel);

		preSetPanel = new JPanel();
		presetSettings = new JLabel("Work Hours schedule:");
		yesRadio = new JRadioButton("Yes");
		noRadio = new JRadioButton("No");
		noRadio.setSelected(true);
		buttonGroup = new ButtonGroup();
		buttonGroup.add(yesRadio);
		buttonGroup.add(noRadio);
		preSetPanel.add(presetSettings);
		preSetPanel.add(yesRadio);
		preSetPanel.add(noRadio);
		settingsPanel.add(preSetPanel);
		
		buttonsPanel = new JPanel();
		applyButton = new JButton("Apply");
		applyButton.addActionListener(this);
		buttonsPanel.add(applyButton);
		settingsPanel.add(buttonsPanel);
		
		//call method to get installed applications
		getInstalledApps();
		
		cp.add(settingsPanel, BorderLayout.NORTH);
		cp.add(scrollable, BorderLayout.CENTER);
		
		frame.setSize(1080, 720);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
				
		//calls the method to get the running processes as well as kill checked processes
		killSelectedApplication();
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {	 
		if(e.getSource()== applyButton){	
			frame2 = new JFrame();
		  	localDate = LocalDate.now(); 
			dayOfWeek = DayOfWeek.from(localDate);
			
			rightNowTime = LocalTime.now();
		  	morningTime = LocalTime.parse("09:00:00"); 
		    eveningTime = LocalTime.parse("18:00:00");
			
			//Message to alert user that applications will be prevented from running.
			JOptionPane.showMessageDialog(frame2, "Applications will now be prevented from running.");
		
			//calls the method that checks which radio buttons are pressed
			checkSelectedRadioButton();
			
			//calls the method to add checked checkboxes into an array list of Strings
			addCheckedApplications();
			
			//for every day checkBoxselected 
			for (JCheckBox checkBox : allDaysOfWeek) {
				if (checkBox.isSelected()) {
					days.add(checkBox.getText());
				}
			}
			//System.out.println(dayOfWeek.name());
			for	(int i=0; i<days.size(); i++) {
				//System.out.println(days.get(i).toLowerCase());
				if(dayOfWeek.name().equalsIgnoreCase(days.get(i))) { 	
					if(yesRadio.isSelected()) {
						if(rightNowTime.isBefore(morningTime) || rightNowTime.isAfter(eveningTime)) {
							stopApps = true;
						}
					}else if(noRadio.isSelected()) {
						stopApps = true;
					}
					
				}
			}
		}
	}	
	public void checkSelectedRadioButton() {
		if(yesRadio.isSelected()) {
			for(int i=0; i<weekDays.size(); i++) {
				weekDays.get(i).setSelected(true);
				//weekDays.get(i).setEnabled(false);
			}
		}else if(noRadio.isSelected()){
			for(int i=0; i<weekDays.size(); i++) {
				weekDays.get(i).setEnabled(true);
			} 
		}
	}
		
	public void addCheckedApplications() {
		for (JCheckBox checkBox : applicationsCheckBoxes) {
			if (checkBox.isSelected()) {
				checkedInstalledApplications.add(checkBox.getText());
			}
		}
	}

	public void getInstalledApps() {
		try {
			String line;
			//runs a command to powershell to the installed app on the device
			Process p = Runtime.getRuntime().exec
					("powershell -command \"Get-ItemProperty HKLM:\\Software\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\* "
							+ "| Select-Object DisplayName | Format-Table ľAutoSize\"");
			//reads the input from the command
			BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
			//goes into a loop to add all the installed applications into an arraylist
			while ((line = input.readLine()) != null) {
				appCount++;
				myInstalledAppsList.add(line);
				
				for(int i = 0; i < myInstalledAppsList.size(); i++) {  
					String currentInstalledApp = myInstalledAppsList.get(i);
					//removes empty strings and certain applications 
					if(currentInstalledApp.isEmpty() ||  currentInstalledApp.isBlank() || currentInstalledApp.contains("-----") || currentInstalledApp.contains("DisplayName")
							|| currentInstalledApp.contains("Microsoft Visual") || currentInstalledApp.contains("ENE") || currentInstalledApp.contains("Windows")
							|| currentInstalledApp.contains("Runtime") || currentInstalledApp.contains("Component") || currentInstalledApp.contains("64")
							|| currentInstalledApp.contains("Installer") || currentInstalledApp.contains("A SIO") || currentInstalledApp.contains("HAL")){
						myInstalledAppsList.remove(i);
					}
				}
			}
			input.close();
			
			//adds the arraylist of strings and inputs them into an arraylist of checkboxes
			for(String element : myInstalledAppsList) {
			    JCheckBox checkboxes = new JCheckBox(element);
			    applicationsCheckBoxes.add(checkboxes);
			    appsPanel.add(checkboxes);
			    
			}
		}catch(Exception err) {
			err.printStackTrace();
		}
	}
	
	public void killSelectedApplication() {
    	//infinitely runs every 5 seconds unless java program is stopped
		java.util.Timer t = new java.util.Timer();
		t.schedule(new TimerTask() {

			@Override
		    public void run() {
		        try {
		        	String process;
		        	// getRuntime: Returns the runtime object associated with the current Java application.
		        	// exec: Executes the specified string command in a separate process.
		        	Process taskList = Runtime.getRuntime().exec(System.getenv("windir") +"\\system32\\"+"tasklist.exe");
		        	BufferedReader tastListInput = new BufferedReader(new InputStreamReader(taskList.getInputStream()));

		        	while ((process = tastListInput.readLine()) != null) {
		        		//System.out.println(process); // <-- Print all Process here line by line
		        		runningApplications.add(process); //adds to the arraylist	
		        					
		        		if(stopApps == true) {
		        			for(int k=0; k<checkedInstalledApplications.size(); k++) {
		        				String[] wordsSplit = checkedInstalledApplications.get(k).split(" ");
		        				//System.out.println(checkedInstalledApplications.get(k));
		        				for (int i=0; i<wordsSplit.length; i++) {
		        					for(int  j=0; j<runningApplications.size(); j++) {
		        						//System.out.println(runningApplications.get(j));
		        						if(runningApplications.get(j).contains(wordsSplit[i].toLowerCase())) {
		        							try {
		        								//slpit string array input and get only the *appname*.exe to compare to checkedInstalledApplications
		        								String[] applicationExe = runningApplications.get(j).split(" ");
		        								Process taskKill = Runtime.getRuntime().exec("taskkill /F /IM " + applicationExe[0]);
		        								runningApplications.remove(j);	
		        								successKill = true;
		        							} catch (IOException e1) {										
		        								e1.printStackTrace();
		        							}
		        							
		        						}  
		        						
		        					}
		        				}
		        			}
		        		}
		        	} 	
		        	
		        	if (successKill) {
			   		     JOptionPane.showMessageDialog(null, "You are being prevented from running this application!", "ERROR!", JOptionPane.ERROR_MESSAGE);
			        }		

		        	tastListInput.close();
		        }catch (Exception err) {
		        	err.printStackTrace();
		        }		
			}			
		}, 5000, 5000);	
	}

}